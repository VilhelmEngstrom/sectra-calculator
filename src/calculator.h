#ifndef CALCULATOR_H
#define CALCULATOR_H

#pragma once
#include "register.h"
#include <array>
#include <cstddef>
#include <string>
#include <string_view>
#include <map>

/* Used for returning information about read input to main */
enum class Directive { Continue, Terminate };

class Calculator {
	public:
		/* Parses the input string, the choice to pass by value is deliberate */
		Directive input(std::string expression);

		static std::string_view constexpr QUIT = std::string_view{"quit"};
		static std::string_view constexpr PRINT = std::string_view{"print"};
		static std::size_t constexpr PRECISION = 20u; /* Precision when printing */

		/* Used when parsing the operator from the input string */
		/* const rather than constexpr to not pollute the header */
		static std::array<std::string_view, 3> const OPERATORS;
	private:
		enum class OperandType { Number, Register };	/* Possible operand types */
		enum class ViewState { Empty, NonEmpty };		/* State of string view after removing first word */

		struct ParseResult {
			ViewState state;
			std::string_view term;
		};

		/* Internal registers of the calculator */
		std::map<std::string, Register> registers_{};

		/* Remove first word from the view and return it. If the given view contains 
		   only a single word, this word will be returned and the ViewState will be set 
           to Empty, otherwise the latter is set to NonEmpty*/
		static ParseResult next_term_and_advance_view(std::string_view& view);

		/* Return Operation enumerator depending on what is in view */
		static Register::Operation parse_operator(std::string_view view);

		/* Evaluate and print register */
		void print(std::string_view reg) const;

		/* Return double corresponding to what is in the view */
		static double as_double(std::string_view view);

		/* Return ref to the register corresponding to the name in the view. */
		/* The reference is needed as we need to check the contents of the register
		   at evaluation. Deleting registers from the map registers_ during run time should 
		   never be done under any circumstances as it would result in dangling references */
		Register const& as_register(std::string_view view);

		/* For validating input */
		static bool is_valid_operand_name(std::string_view name);
		static bool is_valid_operator_name(std::string_view op);
		static bool is_valid_register_name(std::string_view name);
		static bool is_valid_number(std::string_view number);

		/* Operand type of what is in the view */
		static OperandType operand_type_of(std::string_view operand);
};


#endif
